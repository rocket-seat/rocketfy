import React, { useState } from 'react';
import BoardContext from './context';

import List from '../List';
import { Container } from './styles';

import { loadLists } from '../../services/api';
import produce from 'immer';

const data = loadLists();

const Board = () => {

    const [lists, setLists] = useState(data)

    function mover(fromList, toList, from, to) {
        setLists(produce(lists, draft => {
            const dragged = draft[fromList].cards[from]

            draft[fromList].cards.splice(from, 1)
            draft[toList].cards.splice(to, 0, dragged)
        }))
    }

    return (
        <BoardContext.Provider value={{ lists, mover }}>
            <Container>
                {lists.map((item, index) => <List key={item.title} index={index} data={item} />)}
            </Container>
        </BoardContext.Provider>
    )
}

export default Board;
