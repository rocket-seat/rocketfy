import React, { useRef, useContext } from 'react';

import { Container, Label } from './styles';
import { useDrag, useDrop } from 'react-dnd'
import BoardContext from '../Board/context';

const Card = ({ data, index, listIndex }) => {

    const ref = useRef();
    const { mover } = useContext(BoardContext)

    const [{ isDragging }, dragRef] = useDrag({
        item: { type: 'CARD', index, id: data.id, content: data.content, listIndex },
        collect: monitor => ({
            isDragging: monitor.isDragging()
        })
    })

    const [, dropRef] = useDrop({
        accept: 'CARD',
        hover(item, monitor) {
            // console.log(item.id, item.index)
            // console.log(data.id, index)

            const draggedIndex = item.index;
            const targetIndex = index;
            const draggedListIndex = item.listIndex;
            const targetListIndex = listIndex;

            if (draggedIndex === targetIndex && draggedListIndex === targetListIndex) {
                return;
            }

            const targetSize = ref.current.getBoundingClientRect();
            const targetCenter = (targetSize.bottom - targetSize.top) / 2;

            const dragOffset = monitor.getClientOffset();
            const draggedTop = dragOffset.y - targetSize.top;

            if (draggedIndex < targetIndex && draggedTop < targetCenter) {
                return;
            }

            if (draggedIndex > targetIndex && draggedTop > targetCenter) {
                return;
            }

            mover(draggedListIndex, targetListIndex, draggedIndex, targetIndex);

            item.index = targetIndex;
            item.listIndex = targetListIndex;
        }
    })

    dragRef(dropRef(ref))

    return (
        <Container ref={ref} isDragging={isDragging}>
            <header>
                {data.labels.map(item => <Label key={item} color={item} />)}
            </header>

            <p>{data.content}</p>
            {data.user && <img src={data.user} alt="" />}
        </Container>
    )
}

export default Card;
