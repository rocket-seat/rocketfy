import React from 'react';

import { MdAdd } from 'react-icons/md';

import { Container } from './styles';
import Card from '../Card';


const List = ({ data, index: listIndex }) => (
    <Container done={data.done}>
        <header>
            <h2>{data.title}</h2>

            {data.creatable && (
                <button type="button">
                    <MdAdd size={24} color="#fff" />
                </button>
            )}
        </header>

        <ul>
            {data.cards.map((item, index) => <Card key={item.id} listIndex={listIndex} index={index} data={item} />)}
        </ul>
    </Container>
)

export default List;
