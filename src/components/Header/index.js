import React from 'react';

import { Container } from './styles';

const Header = () => (
    <Container>
        <h1>Rocketfy</h1>
    </Container>
)

export default Header;
